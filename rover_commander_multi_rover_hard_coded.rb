require_relative('models/rover_manager.rb')
boundry_size_x = 5
boundry_size_y = 5

mars_rovers = RoverManager.new(boundry_size_x, boundry_size_y)
rover_a = mars_rovers.start_new_rover(1, 2, :N)
rover_b = mars_rovers.start_new_rover(3, 3, :E)

puts "=-==-=-=-=-=- START POSITIONS =-=-=-=-=-=-=-=-=-=-="
puts " ROVER A:: #{mars_rovers.get_position_of rover_a}"
puts " ROVER B:: #{mars_rovers.get_position_of rover_b}"
puts "=-==-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-="
rover_a.move_by 'LMLMLMLMM'
rover_b.move_by 'MMRMMRMRRM'

puts "=-==-=-=-=-=- END POSITIONS PER ROVER =-=-=-=-=-=-="
puts " ROVER A:: #{mars_rovers.get_position_of rover_a}"
puts " ROVER B:: #{mars_rovers.get_position_of rover_b}"
puts "=-==-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-="


puts "=-==-=-=-=- END POSITIONS FOR ASSESMENT =-=-=-=-=-"
mars_rovers.get_position_of_all_rovers
puts "=-==-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-"