require 'spec_helper'
require_relative '../models/rover'

describe 'MoveRover' do

  it 'FacingNorth' do
    rover = Rover.new(1,1, :N)
    rover.move_by 'M'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('1 2 N')
  end

  it 'FacingEast' do
    rover = Rover.new(1,1, :E)
    rover.move_by 'M'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('2 1 E')
  end

  it 'FacingSouth' do
    rover = Rover.new(1,1, :S)
    rover.move_by 'M'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('1 0 S')
  end

  it 'FacingWest' do
    rover = Rover.new(1,1, :W)
    rover.move_by 'M'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('0 1 W')
  end

  it 'FacingWestAndTurningLeft' do
    rover = Rover.new(1,1, :W)
    rover.move_by 'ML'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('0 1 S')
  end

  it 'FacingSouthAndTurningRight' do
    rover = Rover.new(1,1, :S)
    rover.move_by 'MR'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('1 0 W')
  end

  it 'FacingNorthTurningLeftMovingandTurningLeftAgain' do
    rover = Rover.new(1,3, :N)
    rover.move_by 'LML'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('0 3 S')
  end

  it 'FacingEastTurningRightMovingAndTurningRightAgain' do
    rover = Rover.new(3,1, :E)
    rover.move_by 'RML'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('3 0 E')
  end

end

describe 'Do Assesment' do

  it 'UsingInput1' do
    rover = Rover.new(1,2, :N)
    rover.move_by 'LMLMLMLMM'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('1 3 N')
  end

  it 'UsingInput2' do
    rover = Rover.new(3,3, :E)
    rover.move_by 'MMRMMRMRRM'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('5 1 E')
  end

end

describe 'TestingEdgeCasesOnBoundarys' do

  it 'MoveNorthTryToMoveOverBoundaryMaxSizeY' do
    rover = Rover.new(1,2, :N)
    rover.move_by 'LMLMLMLMMMMMMMMML'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('1 5 W')
  end

  it 'MoveEastTryToMoveOverBoundaryMaxSizeX' do
    rover = Rover.new(1,2, :N)
    rover.move_by 'MMRMMMMM'
    rover_position_after_movement = rover.get_position
    expect(rover_position_after_movement).to eq('5 4 E')
  end

end
